package bases;

import org.testng.annotations.BeforeSuite;

public class BaseTest {

    @BeforeSuite(groups = { "regressive", "positive", "negative" })
    protected void setUp() {
        BaseRest.createRequestSpecification();
    }
}
