package configs;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.ConfigCache;

@Config.Sources({
    "classpath:application.properties",
})

public interface EnvironmentConfig extends Config {
    static EnvironmentConfig getInstance() {
        return ConfigCache.getOrCreate(EnvironmentConfig.class);
    }

    @Key("baseUrl")
    String baseUrl();
}
