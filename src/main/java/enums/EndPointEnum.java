package enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EndPointEnum {
    POST_USERS_ENDPOINT("/usuarios"),
    GET_USERS_ENDPOINT("/usuarios"),
    GET_USER_BY_ID_ENDPOINT("/usuarios/{_id}"),
    PUT_USERS_ID_ENDPOINT("/usuarios/{_id}");

    private final String endPoint;
}
