package enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SchemaEnum {
    LIST_REGISTERED_USERS_SCHEMA("/list_registered_users.json"),
    USER_BY_ID_SCHEMA("/search_user_by_id_schema.json");

    private final String schema;
}
