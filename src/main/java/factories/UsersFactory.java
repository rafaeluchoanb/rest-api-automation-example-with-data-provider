package factories;

import models.requests.User;

import java.util.UUID;

public class UsersFactory {

    public static User buildUser(String password, String administrador) {
        String uuid = UUID.randomUUID().toString();
        return User.builder()
                .nome(uuid)
                .email(uuid.concat("@teste.com"))
                .password(password)
                .administrador(administrador)
                .build();
    }

    public static User buildUser() {
        return buildUser("123456", "true");
    }
}