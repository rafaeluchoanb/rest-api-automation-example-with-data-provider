package providers;

import factories.UsersFactory;
import models.requests.User;
import org.testng.annotations.DataProvider;
import services.UsersRest;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static java.util.Objects.isNull;


public class ListRegisteredUsersProvider {
    @DataProvider(name = "listUsersWithoutQueryParametersSuccessfullyProvider")
    public Object[][] getListUsersWithoutQueryParametersSuccessfullyProvider() {
        UsersRest.postUsers(UsersFactory.buildUser(), 201);
        return new Object[][] {
                {
                        createQueryParams(null, null, null, null, null)
                }
        };
    }

    @DataProvider(name = "listUsersWithQueryParametersSuccessfullyProvider")
    public Object[][] getListUsersWithQueryParametersSuccessfullyProvider() {
        User user = UsersFactory.buildUser();
        String id = UsersRest.postUsers(user, 201).extract().response().body().path("_id").toString();
        return new Object[][] {
                {
                        createQueryParams(id, null , null, null, null)
                },
                {
                        createQueryParams(null, user.getNome().toString(), null, null, null)
                },
                {
                        createQueryParams(null, null , user.getEmail().toString(), null, null)
                },
                {
                        createQueryParams(null, null , null, user.getPassword().toString(), null)
                },
                {
                        createQueryParams(null, null , null, null, user.getAdministrador().toString())
                },
        };
    }

    @DataProvider(name = "successfullyListUsersWithEmptyQueryParametersValuesProvider")
    public Object[][] getSuccessfullyListUsersWithEmptyQueryParametersValuesProvider() {
        User user = UsersFactory.buildUser();
        UsersRest.postUsers(user, 201);
        return new Object[][] {
                {
                        createQueryParams("", "", user.getEmail().toString(), "", user.getAdministrador().toString())
                }
        };
    }

    @DataProvider(name = "invalidEmailFieldProvider")
    public Object[][] getInvalidEmailFieldProvider() {
        return new Object[][] {
                {
                        createQueryParams(null, null, "usuario@", null, null),
                        "email deve ser um email válido"
                },
                {
                        createQueryParams(null, null, "@dominio.com", null, null),
                        "email deve ser um email válido"
                },
                {
                        createQueryParams(null, null, "usuariodominio.com", null, null),
                        "email deve ser um email válido"
                },
                {
                        createQueryParams(null, null, "usuario@dominio", null, null),
                        "email deve ser um email válido"
                }
        };
    }

    @DataProvider(name = "invalidAdministradorFieldProvider")
    public Object[][] getInvalidAdministradorFieldProvider() {
        return new Object[][] {
                {
                        createQueryParams(null, null, null, null, "verdadeiro"),
                        "administrador deve ser 'true' ou 'false'"
                },
                {
                        createQueryParams(null, null, null, null, "falso"),
                        "administrador deve ser 'true' ou 'false'"
                }
        };
    }

    @DataProvider(name = "noUsersFoundProvider")
    public Object[][] getNoUsersFoundProvider() {
        return new Object[][] {
                {
                        createQueryParams(UUID.randomUUID().toString(), null, null, null, null)
                }
        };
    }

    private Map<String, Object> createQueryParams(String id, String nome, String email, String password, String administrador) {
        Map<String, Object> queryParam = new HashMap<>();

        addQueryParamIfNotNull(queryParam, "_id", id);
        addQueryParamIfNotNull(queryParam, "nome", nome);
        addQueryParamIfNotNull(queryParam, "email", email);
        addQueryParamIfNotNull(queryParam, "password", password);
        addQueryParamIfNotNull(queryParam, "administrador", administrador);

        return queryParam;
    }

    private void addQueryParamIfNotNull(Map<String, Object> queryParam, String key, Object value) {
        if (!isNull(value)) {
            queryParam.put(key, value);
        }
    }
}