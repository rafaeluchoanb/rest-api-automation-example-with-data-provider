package providers;

import models.requests.User;
import org.testng.annotations.DataProvider;

import java.util.UUID;

import static factories.UsersFactory.buildUser;

public class RegisterUserProvider {

    @DataProvider(name = "registerUserSuccessfullyProvider")
    public Object[][] getRegisterUserSuccessfullyProvider() {
        return new Object[][] {
                {
                        buildUser()
                }
        };
    }

    @DataProvider(name = "requiredFieldsProvider")
    public Object[][] getRequiredFieldsProvider() {
        String uuid = UUID.randomUUID().toString();
        return new Object[][] {
                {
                        User.builder()
                                .email(uuid.concat("@teste.com"))
                                .password("123456")
                                .administrador("true")
                                .build(),
                        "nome",
                        "nome é obrigatório"
                },
                {
                        User.builder()
                                .nome(uuid)
                                .password("123456")
                                .administrador("true")
                                .build(),
                        "email",
                        "email é obrigatório"
                },
                {
                        User.builder()
                                .nome(uuid)
                                .email(uuid.concat("@teste.com"))
                                .administrador("true")
                                .build(),
                        "password",
                        "password é obrigatório"
                },
                {
                        User.builder()
                                .nome(uuid)
                                .email(uuid.concat("@teste.com"))
                                .password("123456")
                                .build(),
                        "administrador",
                        "administrador é obrigatório"
                }
        };
    }

    @DataProvider(name = "invalidTypesProvider")
    public Object[][] getInvalidTypesProvider() {
        String uuid = UUID.randomUUID().toString();
        return new Object[][] {
                {
                        User.builder()
                                .nome(1)
                                .email(uuid.concat("@teste.com"))
                                .password("123456")
                                .administrador("true")
                                .build(),
                        "nome",
                        "nome deve ser uma string"
                },
                {
                        User.builder()
                                .nome(uuid)
                                .email(1)
                                .password("123456")
                                .administrador("true")
                                .build(),
                        "email",
                        "email deve ser uma string"
                },
                {
                        User.builder()
                                .nome(uuid)
                                .email(uuid.concat("@teste.com"))
                                .password(1)
                                .administrador("true")
                                .build(),
                        "password",
                        "password deve ser uma string"
                },
                {
                        User.builder()
                                .nome(uuid)
                                .email(uuid.concat("@teste.com"))
                                .password("123456")
                                .administrador(1)
                                .build(),
                        "administrador",
                        "administrador deve ser 'true' ou 'false'"
                }
        };
    }

    @DataProvider(name = "invalidEmailValueProvider")
    public Object[][] getInvalidEmailValueProvider() {
        String uuid = UUID.randomUUID().toString();
        return new Object[][] {
                {
                        User.builder()
                                .nome(uuid)
                                .email(uuid.concat("teste.com"))
                                .password("123456")
                                .administrador("true")
                                .build()
                },
                {
                        User.builder()
                                .nome(uuid)
                                .email(uuid.concat("@testecom"))
                                .password("123456")
                                .administrador("true")
                                .build()
                },
                {
                        User.builder()
                                .nome(uuid)
                                .email(uuid.concat("@teste."))
                                .password("123456")
                                .administrador("true")
                                .build()
                }
        };
    }

    @DataProvider(name = "registeredEmailProvider")
    public Object[][] getRegisteredEmailProvider() {
        return new Object[][] {
                {
                        buildUser()
                }
        };
    }
}
