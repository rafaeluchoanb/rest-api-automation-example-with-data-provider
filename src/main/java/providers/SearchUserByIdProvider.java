package providers;

import models.requests.User;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import services.UsersRest;

import static factories.UsersFactory.buildUser;


public class SearchUserByIdProvider {

    @DataProvider(name = "searchUserByIdSuccessfullyProvider")
    public Object[][] getSearchUserByIdSuccessfullyProvider() {
        User user = buildUser();
        return new Object[][] {
                {
                        UsersRest.postUsers(user, HttpStatus.SC_CREATED).extract().response().path("_id"),
                        user
                }
        };
    }

    @DataProvider(name = "nonExistentUserProvider")
    public Object[][] getNonExistentUserProvider() {
        return new Object[][] {
                {
                        "Usuário não encontrado"
                }
        };
    }
}