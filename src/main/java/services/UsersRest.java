package services;

import io.restassured.response.ValidatableResponse;
import models.requests.User;

import java.util.Map;

import static enums.EndPointEnum.*;
import static io.restassured.RestAssured.given;
import static java.util.Objects.isNull;

public class UsersRest {

    public static ValidatableResponse postUsers(User user, int statusCode) {
        return given()
                .body(user)
                .post(POST_USERS_ENDPOINT.getEndPoint())
                .then()
                .statusCode(statusCode);
    }

    public static ValidatableResponse getUserById(String id, int statusCode) {
        return given()
                .pathParam("_id", id)
                .get(GET_USER_BY_ID_ENDPOINT.getEndPoint())
                .then()
                .statusCode(statusCode);
    }

    public static ValidatableResponse putUsersById(User user, Object id, int statusCode) {
        id = (!isNull(id)) ? id : "";

        return given()
                .body(user)
                .pathParam("_id", id)
                .put(PUT_USERS_ID_ENDPOINT.getEndPoint())
                .then()
                .statusCode(statusCode);
    }

    public static ValidatableResponse putUsersById(User user, int statusCode) {
        return putUsersById(user, null, statusCode);
    }

    public static ValidatableResponse getUsers(Map<String, Object> queryParams, int statusCode) {
        return given()
                .queryParams(queryParams)
                .get(GET_USERS_ENDPOINT.getEndPoint())
                .then()
                .statusCode(statusCode);
    }
}
