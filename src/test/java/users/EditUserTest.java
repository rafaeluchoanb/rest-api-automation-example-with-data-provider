package users;

import bases.BaseTest;
import io.restassured.response.Response;
import models.requests.User;
import org.apache.http.HttpStatus;
import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;
import providers.EditUserProvider;
import services.UsersRest;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

@Test(groups = { "regressive" })
public class EditUserTest extends BaseTest {

    @Test(description = "Edit user successfully",
            dataProviderClass = EditUserProvider.class,
            dataProvider = "editUserSuccessfullyProvider",
            groups = { "positive" }
            )
    public void testEditUserSuccessfully(User requestBodyRegisterUser, User requestBodyEditUser) {
        String id = UsersRest.postUsers(requestBodyRegisterUser, HttpStatus.SC_CREATED).extract().response().body().path("_id");
        Response responseBodyPut = UsersRest.putUsersById(requestBodyEditUser, id, HttpStatus.SC_OK).extract().response();

        assertEquals("Registro alterado com sucesso", responseBodyPut.body().path("message"));

        User responseBodyGet = UsersRest.getUserById(id, HttpStatus.SC_OK)
                .extract().response().as(User.class);

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(responseBodyGet)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(requestBodyEditUser);

        assertEquals(id, responseBodyGet.getId().toString());
        softAssertions.assertAll();
    }

    @Test(description = "Register user successfully when id not found",
            dataProviderClass = EditUserProvider.class,
            dataProvider = "registerUserSuccessfullyWhenIdNotFoundProvider",
            groups = { "positive" }
    )
    public void testRegisterUserWhenIdNotFound(User requestBody, String nonExistentId) {
        Response responseBodyPost = UsersRest.putUsersById(requestBody, nonExistentId, HttpStatus.SC_CREATED)
                .extract().response();
        String id = responseBodyPost.body().path("_id");

        assertEquals( "Cadastro realizado com sucesso", responseBodyPost.body().path("message"));
        assertNotNull(id);

        User responseBodyGet = UsersRest.getUserById(id, HttpStatus.SC_OK)
                .extract().response().as(User.class);

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(responseBodyGet)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(requestBody);

        assertEquals(id, responseBodyGet.getId().toString());
        softAssertions.assertAll();
    }

    @Test(description = "Required fields",
            dataProviderClass = EditUserProvider.class,
            dataProvider = "requiredFieldsProvider",
            groups = { "negative" }
            )
    public void testRequiredFields(User requestBody, String field, String expectedErrorMessage) {
        String actualErrorMessage = UsersRest.putUsersById(requestBody, "requiredFields", HttpStatus.SC_BAD_REQUEST)
                .extract().response().body().path(field);

        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test(description = "Invalid types",
            dataProviderClass = EditUserProvider.class,
            dataProvider = "invalidTypesProvider",
            groups = { "negative" }
            )
    public void testInvalidTypes(User requestBody, String field, String expectedErrorMessage) {
        String actualErrorMessage = UsersRest.putUsersById(requestBody, "requiredFields", HttpStatus.SC_BAD_REQUEST)
                .extract().response().body().path(field);

        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test(description = "Invalid email value",
            dataProviderClass = EditUserProvider.class,
            dataProvider = "invalidEmailValueProvider",
            groups = { "negative" }
            )
    public void testInvalidEmailValue(User requestBody) {
        String actualErrorMessage = UsersRest.putUsersById(requestBody, "invalidEmailValue",HttpStatus.SC_BAD_REQUEST)
                .extract().response().body().path("email");

        assertEquals("email deve ser um email válido", actualErrorMessage);
    }

    @Test(description = "Registered email",
            dataProviderClass = EditUserProvider.class,
            dataProvider = "registeredEmailProvider",
            groups = { "negative" }
    )
    public void testRegisteredEmail(User requestBody) {
        UsersRest.postUsers(requestBody, HttpStatus.SC_CREATED);
        String actualErrorMessage = UsersRest.putUsersById(requestBody, "registeredEmail", HttpStatus.SC_BAD_REQUEST)
                .extract().response().body().path("message");

        assertEquals("Este email já está sendo usado", actualErrorMessage);
    }

    @Test(description = "No id path param",
            dataProviderClass = EditUserProvider.class,
            dataProvider = "noIdPathParamProvider",
            groups = { "negative" }
    )
    public void testNoIdPathParam(User requestBody) {
        String actualErrorMessage = UsersRest.putUsersById(requestBody, HttpStatus.SC_METHOD_NOT_ALLOWED)
                .extract().response().body().path("message");

        assertEquals("Não é possível realizar PUT em /usuarios/. Acesse https://serverest.dev para ver as rotas disponíveis e como utilizá-las.",
                actualErrorMessage);
    }
}
