package users;

import bases.BaseTest;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import providers.ListRegisteredUsersProvider;
import services.UsersRest;

import java.util.Map;

import static enums.SchemaEnum.LIST_REGISTERED_USERS_SCHEMA;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.AssertJUnit.assertEquals;

@Test(groups = { "regressive" })
public class ListRegisteredUsersTest extends BaseTest {

    @Test(description = "list users without query parameters successfully",
            dataProviderClass = ListRegisteredUsersProvider.class,
            dataProvider = "listUsersWithoutQueryParametersSuccessfullyProvider",
            groups = { "positive" }
    )
    public void testListUsersWithoutQueryParameterSuccessfully(Map<String, Object> queryParams) {
        UsersRest.getUsers(queryParams, HttpStatus.SC_OK)
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schemas" + LIST_REGISTERED_USERS_SCHEMA.getSchema()));
    }

    @Test(description = "list users with query parameter successfully",
            dataProviderClass = ListRegisteredUsersProvider.class,
            dataProvider = "listUsersWithQueryParametersSuccessfullyProvider",
            groups = { "positive" }
    )
    public void testListUsersWithQueryParametersSuccessfully(Map<String, Object> queryParams) {
        UsersRest.getUsers(queryParams, HttpStatus.SC_OK)
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schemas" + LIST_REGISTERED_USERS_SCHEMA.getSchema()));
    }

    @Test(description = "Successfully list users with empty query parameters values",
            dataProviderClass = ListRegisteredUsersProvider.class,
            dataProvider = "successfullyListUsersWithEmptyQueryParametersValuesProvider",
            groups = { "positive" }
    )
    public void testSuccessfullyListUsersWithEmptyQueryParametersValues(Map<String, Object> queryParams) {
        UsersRest.getUsers(queryParams, HttpStatus.SC_OK)
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schemas" + LIST_REGISTERED_USERS_SCHEMA.getSchema()));
    }

    @Test(description = "No users found",
            dataProviderClass = ListRegisteredUsersProvider.class,
            dataProvider = "noUsersFoundProvider",
            groups = { "positive" }
    )
    public void testNoUsersFoundProvider(Map<String, Object> queryParams) {
        Response response = UsersRest.getUsers(queryParams, HttpStatus.SC_OK).extract().response();

        assertEquals(Integer.parseInt(response.body().path("quantidade").toString()), 0);
        assertThat(response.body().path("usuarios"), Matchers.empty());
    }

    @Test(description = "Invalid email field",
            dataProviderClass = ListRegisteredUsersProvider.class,
            dataProvider = "invalidEmailFieldProvider",
            groups = { "negative" }
    )
    public void testInvalidEmailField(Map<String, Object> queryParams, String actualErrorMessage) {
        String expectedErrorMensage = UsersRest.getUsers(queryParams, HttpStatus.SC_BAD_REQUEST).extract().response().body().path("email");

        assertEquals(expectedErrorMensage,
                actualErrorMessage);
    }

    @Test(description = "Invalid administrador field",
            dataProviderClass = ListRegisteredUsersProvider.class,
            dataProvider = "invalidAdministradorFieldProvider",
            groups = { "negative" }
    )
    public void testInvalidAdministradorField(Map<String, Object> queryParams, String actualErrorMessage) {
        String expectedErrorMensage = UsersRest.getUsers(queryParams, HttpStatus.SC_BAD_REQUEST).extract().response().body().path("administrador");

        assertEquals(expectedErrorMensage,
                actualErrorMessage);
    }
}
