package users;

import bases.BaseTest;
import io.restassured.response.Response;
import models.requests.User;
import org.apache.http.HttpStatus;
import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;
import providers.RegisterUserProvider;
import services.UsersRest;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

@Test(groups = { "regressive" })
public class RegisterUsersTest extends BaseTest {

    @Test(description = "Register user successfully",
            dataProviderClass = RegisterUserProvider.class,
            dataProvider = "registerUserSuccessfullyProvider",
            groups = { "positive" }
            )
    public void testRegisterUserSuccessfully(User requestBody) {
        Response responseBodyPost = UsersRest.postUsers(requestBody, HttpStatus.SC_CREATED)
                .extract().response();
        String id = responseBodyPost.body().path("_id");

        assertEquals( "Cadastro realizado com sucesso", responseBodyPost.body().path("message"));
        assertNotNull(id);

        User responseBodyGet = UsersRest.getUserById(id, HttpStatus.SC_OK)
                .extract().response().as(User.class);

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(responseBodyGet)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(requestBody);

        assertEquals(id, responseBodyGet.getId().toString());
        softAssertions.assertAll();
    }

    @Test(description = "Required fields",
            dataProviderClass = RegisterUserProvider.class,
            dataProvider = "requiredFieldsProvider",
            groups = { "negative" }
            )
    public void testRequiredFields(User requestBody, String field, String expectedErrorMessage) {
        String actualErrorMessage = UsersRest.postUsers(requestBody, HttpStatus.SC_BAD_REQUEST)
                .extract().response().body().path(field);

        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test(description = "Invalid types",
            dataProviderClass = RegisterUserProvider.class,
            dataProvider = "invalidTypesProvider",
            groups = { "negative" }
            )
    public void testInvalidTypes(User requestBody, String field, String expectedErrorMessage) {
        String actualErrorMessage = UsersRest.postUsers(requestBody, HttpStatus.SC_BAD_REQUEST)
                .extract().response().body().path(field);

        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test(description = "Invalid email value",
            dataProviderClass = RegisterUserProvider.class,
            dataProvider = "invalidEmailValueProvider",
            groups = { "negative" }
            )
    public void testInvalidEmailValue(User requestBody) {
        String actualErrorMessage = UsersRest.postUsers(requestBody, HttpStatus.SC_BAD_REQUEST)
                .extract().response().body().path("email");

        assertEquals("email deve ser um email válido", actualErrorMessage);
    }

    @Test(description = "Registered email",
            dataProviderClass = RegisterUserProvider.class,
            dataProvider = "registeredEmailProvider",
            groups = { "negative" }
    )
    public void testRegisteredEmail(User requestBody) {
        UsersRest.postUsers(requestBody, HttpStatus.SC_CREATED);
        String actualErrorMessage = UsersRest.postUsers(requestBody, HttpStatus.SC_BAD_REQUEST)
                .extract().response().body().path("message");

        assertEquals("Este email já está sendo usado", actualErrorMessage);
    }
}
