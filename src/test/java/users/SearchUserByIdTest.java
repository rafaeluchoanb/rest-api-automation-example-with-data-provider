package users;

import bases.BaseTest;
import io.restassured.module.jsv.JsonSchemaValidator;
import models.requests.User;
import org.apache.http.HttpStatus;
import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;
import providers.SearchUserByIdProvider;
import services.UsersRest;

import static enums.SchemaEnum.USER_BY_ID_SCHEMA;
import static org.testng.AssertJUnit.assertEquals;

@Test(groups = { "regressive" })
public class SearchUserByIdTest extends BaseTest {

    @Test(description = "Search user by id successfully",
            dataProviderClass = SearchUserByIdProvider.class,
            dataProvider = "searchUserByIdSuccessfullyProvider",
            groups = { "positive" }
            )
    public void testSearchUserByIdSuccessfully(String id, User requestBody) {
        User responseBody = UsersRest.getUserById(id, HttpStatus.SC_OK)
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schemas" + USER_BY_ID_SCHEMA.getSchema()))
                .extract().response().as(User.class);

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(responseBody)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(requestBody);

        assertEquals(id, responseBody.getId());
        softAssertions.assertAll();
    }

    @Test(description = "Non-existent user",
            dataProviderClass = SearchUserByIdProvider.class,
            dataProvider = "nonExistentUserProvider",
            groups = { "negative" }
    )
    public void testNonExistentUser(String expectedMessage) {
        String actualErrorMessage = UsersRest.getUserById("nonExistentUser", HttpStatus.SC_BAD_REQUEST)
                .extract().response().body().path("message");

        assertEquals(expectedMessage, actualErrorMessage);
    }
}